#include <stdio.h>
#include <stdlib.h>
 
using namespace std;
 
int main() {

    int a, b;
    float Tpg;
    
    scanf("%d", &a);
    scanf("%d", &b);
    
    Tpg = (float) (a * b)/12;
    
    printf("%.3f\n", Tpg);
     
    return 0;
}