#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct tm {
    int tm_sec;
    int tm_min;
    int tm_hour;
    int tm_mday:
    int tm_mon;
    int tm_year;
    int tm_wday;
    int tm_yday;
    int tm_isdst;
};

int main(void)
{
    struct tm *data_hora_atual;

    time_t segundos;

    time(&segundos);

    data_hora_atual = localtime(&segundos);

    printf("\nDia..........: %d\n", data_hora_atual->tm_mday);

    printf("\nMes..........: %d\n", data_hora_atual->tm_mon+1);

    printf("\nAno..........: %d\n\n", data_hora_atual->tm_year+1900);

    printf("\nDia do ano...: %d\n", data_hora_atual->tm_yday);
    printf("\nDia da semana: %d\n\n", data_hora_atual->tm_wday);

    printf("\nHora ........: %d:",data_hora_atual->tm_hour);//hora
    printf("%d:",data_hora_atual->tm_min);//minuto
    printf("%d\n",data_hora_atual->tm_sec);//segundo

    printf("\nData ........: %d/", data_hora_atual->tm_mday);
    printf("%d/",data_hora_atual->tm_mon+1); //m�s
    printf("%d\n\n",data_hora_atual->tm_year+1900); //ano

    return 0;
}
