#include <stdio.h>
#include <stdlib.h>

/*
Pergunta 3

Fa�a um programa que leia um n�mero e retorne o fatorial deste n�mero.
*/

int main(){
    int num, fatorial;
    printf("Digite um numero: ");
    scanf("%d", &num);
    for(fatorial = 1; num > 1; num = num - 1){
        fatorial = fatorial * num;
    }
    printf("\n O fatorial e %d", fatorial);
    return 0;
}
