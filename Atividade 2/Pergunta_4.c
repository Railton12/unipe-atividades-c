#include <stdio.h>
#include <stdlib.h>

/*
Pergunta 4

Elabore um programa em linguagem C que receba um ano (num�rico inteiro)
e informe se o ano � bissexto ou n�o (anos bissextos s�o m�ltiplos de 4
portanto, se a divis�o do ano por 4 gerar resto igual a zero, o ano � bissexto - use o operador %).
*/

int main(){
    int ano;

    printf("Informe um Ano: ");
    scanf("%d", &ano);

    if(ano % 4 == 0){
        printf("O ano e bissexto %d");
    } else {
        printf("O ano nao bissexto%d");
    }
    return 0;
}
