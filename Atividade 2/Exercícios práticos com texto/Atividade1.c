#include <stdio.h>
#include <stdlib.h>

/*
Implemente a fun��o strcpy de string.h utilizando qualquer estrutura de repeti��o.
A fun��o strcpy copia o conte�do de uma string para outra. DICA: Cuidado com o espa�o em mem�ria
*/

int main() {
    int i;
    char str1[20];
    char str2[20];

    printf("Digite alguma palavra com no maximo 20 letras): ");
    scanf("%s", str1);

    for(i = 0; i < sizeof(str1); i++){
        str2[i] = str1[i];
    }
    printf("\nA primeira palavra digitada foi : %s\n", str1);
    printf("A segunda palavra foi : %s\n", str2);
}
