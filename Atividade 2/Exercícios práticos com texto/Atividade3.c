#include <stdio.h>
#include <stdlib.h>

/*
Implemente a fun��o strcat de string.h utilizando qualquer estrutura de repeti��o.
A fun��o strcat concatena o conte�do de uma string em outra. DICA: Cuidado com o espa�o em mem�ria.
*/

int main() {
    char str1[40];
    char str2[21];
    int i, j = 0;

    printf("Digite uma palavra com no maximo 20 letras\n");
    scanf("%s", str1);
    scanf("%s", str2);

	for(i = 0; str1[i] != '\0' && i < sizeof(str1); i++) {}

	for(j = 0; str2[j] != '\0' && j < sizeof(str2); j++, i++) {
		str1[i] = str2[j];
	}

	printf("A palavra concatenada e: %s", str1);

	return 0;
}
