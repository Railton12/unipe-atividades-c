#include <stdio.h>
#include <stdlib.h>

/*
Implemente a fun��o strcmp de string.h utilizando qualquer estrutura de repeti��o.
A fun��o strcmp compara os conte�dos de duas strings e verifica se s�o iguais retornando 0.
*/

int main(){
    char str1[20];
    char str2[20];
    int i;
    int retorne = 0;

    printf("Digite uma palavra com no maximo 20 letras");
    scanf("%s", str1);
    scanf("%s", str2);

    for(i = 0; str1[i] != '\0'; i++){
        if(str1[i] != str2[i]){
            retorne = 1;
            break;
        }
    }

    for(i = 0; str2[i] != '\0'; i++){
        if(str1[i] != str2[i]){
            retorne = 1;
            break;
        }
    }

    if(retorne == 0){
        printf("\nAs palavras sao iguais, Valor de retorno = %d",retorne);
    }
    else {
        printf("\nAs palavras sao diferente, Valor de retorno %d",retorne);
    }

    return 0;
}
