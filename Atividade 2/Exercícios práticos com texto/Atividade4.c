#include <stdio.h>
#include <stdlib.h>

/*
Implemente a fun��o strlen de string.h utilizando qualquer estrutura de repeti��o.
A fun��o strlen retorna a quantidade de caracteres em uma string. DICA: A string termina com o caractere especial '\0'.
*/
int main (){
    char str[20];
    int i, cont = 0;

    printf("Digite uma palavra com no maximo 20 letras)");
    scanf("%s", str);

    for(i =0; str[i] != '\0' && i < sizeof(str); i++) {
        cont++;
    }

    printf("A palavra tem %d caracteres.", cont);

    return 0;
}
