#include <stdio.h>
#include <stdlib.h>

/*
Pal�ndromo � a frase ou palavra que se pode ler, indiferentemente
da esquerda para a direita ou vice-versa, como por exemplo a palavra ARARA ou o nome ANA.

Escreva um programa que receba do usu�rio um texto e informe se � pal�ndromo ou n�o.
Desconsidere caracteres em ma�usculo, por exemplo, o nome Ana deve ser identificado como pal�ndromo.
*/

int main() {
    char string[20], reversa[20];
    int i, k, len, retorno = 0;

    printf("Digite uma palavra com no maximo 20 letras\n");
    printf("Digite: ");
    scanf("%s", string);

    while(string[len] != '\0')len++;

    for(i = len, k = 0; i > 0; i--, k++){
        reversa[k] = string[i-1];
    }

    for(i = 0; i < len; i++){
        if(string[i] != reversa[i]){
            retorno = 1;
            break;
        }
    }

    if(retorno == 0){
        printf("\nA string, %s, e' Palindromo.", string);
    }
    else {
        printf("\nA string, %s, nao e' Palindromo.", string);
    }

    return 0;
}
