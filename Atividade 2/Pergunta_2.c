#include <stdio.h>
#include <stdlib.h>

/*
Pergunta 2

Fa�a um programa que leia um n�mero e diga se esse n�mero � par ou n�o.
*/

int main(){
    int num;
    printf("Digite um numero inteiro ");
    scanf("%d", &num);
    if(num%2==0){
        printf("Par \n");
    }
    else{
        printf("Nao e par \n");
    }
    return 0;
}
