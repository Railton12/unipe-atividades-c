#include <stdio.h>
#include <stdlib.h>

/*
Pergunta 6

Fa�a um programa que receba do usu�rio a quantidade de elementos de uma Progress�o Geom�trica (PG)
e a raz�o, gere uma PG em lista com a quantidade de elementos definido pelo usu�rio e imprima.
Uma PG tem como primeiro elemento o n�mero 1 e o pr�ximo elemento � o anterior multiplicado da raz�o.

Exemplo de uma PG com raz�o 2:

1, 2, 4, 8, 16...
*/

int main(){
    int al, r, i, pg = 1;

    printf("Digite uma quantidade de numeros: ");
    scanf("%d", &al);
    printf("Digite a Razao: ");
    scanf("%d", &r);
    for(i = 0; i < al; i++){
        printf("%d ", pg);
        pg *= r;
    }
    return 0;
}
