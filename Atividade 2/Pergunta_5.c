#include <stdio.h>
#include <stdlib.h>

/*
Pergunta 5

Fa�a um programa que receba do usu�rio a quantidade de elementos de uma Progress�o Aritm�tica (PA)
e a raz�o, gere uma PA em lista com a quantidade de elementos definido pelo usu�rio e imprima.
Uma PA tem como primeiro elemento o n�mero 0 e o pr�ximo elemento � o anterior somado da raz�o.

Exemplo de uma PA com raz�o 3:
0, 3, 6, 9, 12...
*/

int main(){
    int al, i, r, n = 0;
    printf("Digite uma quantidade de numeros: ");
    scanf("%d", &al);

    printf("Digite a Razao: ");
    scanf("%d", &r);

    for(i = 0; i < al; i++){
        printf("%d ", n);
        n += r;
    }
    return 0;
}
